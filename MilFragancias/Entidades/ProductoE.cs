﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    [DataContract]
    public class ProductoE
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Producto { get; set; }
        [DataMember]
        public string Descripcion { get; set; }
        [DataMember]
        public int Cantidad { get; set; }
        [DataMember]
        public TipoProductoE Tipo { get; set; }
        [DataMember]
        public float Precio { get; set; }
        [DataMember]
        public ImagenE Imagen { get; set; }
        [DataMember]
        public char Activo { get; set; }

    }
}
