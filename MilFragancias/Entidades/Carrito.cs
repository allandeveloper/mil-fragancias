﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    [DataContract]
    public class Carrito
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public UsuarioE Usuario { get; set; }
        [DataMember]
        public ProductoE Producto { get; set; }
        [DataMember]
        public int Cantidad { get; set; }
        [DataMember]
        public float Precio { get; set; }
    }
}
