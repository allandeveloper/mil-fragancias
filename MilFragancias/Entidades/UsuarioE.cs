﻿using System.Runtime.Serialization;

namespace Entidades
{
    [DataContract]
    public class UsuarioE
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Usuario { get; set; }
        [DataMember]
        public string Correo { get; set; }
        [DataMember]
        public string Contrasenna { get; set; }
        [DataMember]
        public char Activo { get; set; }
        [DataMember]
        public int Admin { get; set; }
    }

}