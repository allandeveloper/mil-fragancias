﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    [DataContract]
    public class TipoProductoE
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Tipo { get; set; }
        [DataMember]
        public char Activo { get; set; }

        public override string ToString()
        {
            return Tipo;
        }
    }
}
