﻿using System.Runtime.Serialization;

namespace Entidades
{
    [DataContract]
    public class ImagenE
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public byte[] Imagen { get; set; }
        [DataMember]
        public char Activo { get; set; }
    }
}
