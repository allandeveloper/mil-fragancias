﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Entidades;
using Logica;
namespace MilFraganciasService
{
    public class MilFragancias : IUsuarioBL, IProductoBL
    {
        public UsuarioE AuntentificarUsuario(UsuarioE usuario)
        {
            UsuarioBL ubl = new UsuarioBL();
            return ubl.AuntentificarUsuario(usuario);

        }

        public ProductoE BuscarProducto(int idProducto)
        {
            ProductoBL pbl = new ProductoBL();
            return pbl.BuscarProducto(idProducto);
        }

        public UsuarioE BuscarUsuario(int idUsuario)
        {
            UsuarioBL ubl = new UsuarioBL();
            return ubl.BuscarUsuario(idUsuario);
        }

        public int EditarProducto(ProductoE producto)
        {
            ProductoBL pbl = new ProductoBL();
            return pbl.EditarProducto(producto);
        }

        public int EditarUsuario(UsuarioE usuario)
        {
            UsuarioBL ubl = new UsuarioBL();
            return ubl.EditarUsuario(usuario);
        }

        public int EliminarProducto(int idProducto)
        {
            ProductoBL pbl = new ProductoBL();
            return pbl.EliminarProducto(idProducto);
        }

        public int EliminarUsuario(int idUsuario)
        {
            UsuarioBL ubl = new UsuarioBL();
            return ubl.EliminarUsuario(idUsuario);
        }

        public List<ProductoE> MostrarProductos(bool todos)
        {
            ProductoBL pbl = new ProductoBL();
            return pbl.MostrarProductos(todos);
        }

        public IEnumerable<TipoProductoE> MostrarTipos()
        {
            ProductoBL pbl = new ProductoBL();
            return pbl.MostrarTipos();
        }

        public IEnumerable<UsuarioE> MostrarUsuarios()
        {
            UsuarioBL ubl = new UsuarioBL();
            return ubl.MostrarUsuarios();
        }

        public int NuevoProducto(ProductoE producto)
        {
            ProductoBL pbl = new ProductoBL();
            return pbl.NuevoProducto(producto);
        }

        public int NuevoUsuario(UsuarioE usuario)
        {
            UsuarioBL ubl = new UsuarioBL();
            return ubl.NuevoUsuario(usuario);
        }
    }
}
