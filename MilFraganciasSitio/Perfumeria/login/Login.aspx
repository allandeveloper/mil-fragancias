﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="login_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery-3.0.0.min.js"></script>
    <link href="../css/login.css" rel="stylesheet" />
    <link href="../css/navbar.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="#">Mil Fragancias</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="Login.aspx">Iniciar Sesión</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Registro.aspx">Registrar</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <br />
    <br />
    <div class="login-popup-wrap new_login_popup">
        <div class="login-popup-heading text-center">
            <h4><i class="fa fa-lock" aria-hidden="true"></i>Iniciar Sesión </h4>
        </div>
        <!--<form accept-charset="utf-8" method="post" action="">-->
        <form runat="server" id="loginMember" role="form" action="" method="post">
            <div class="form-group">
                <asp:TextBox runat="server" type="text" class="form-control" ID="user_id" placeholder="Correo Electrónico" name="user_id"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:TextBox runat="server" type="password" class="form-control" ID="password" placeholder="Contraseña" name="password"></asp:TextBox>
            </div>
            <asp:Button runat="server" type="submit" class="btn btn-default login-popup-btn" name="submit" value="1" Text="Iniciar Sesión" OnClick="Login_Click"></asp:Button>
        </form>
        <div class="form-group text-center">
            <a class="pwd-forget" href="javascript:void(0)" id="open_forgotPassword">Olvido su contraseña</a>
        </div>
        <div class="text-center">No tienes una cuenta? <a href="#">Click aquí</a></div>
    </div>
</body>
</html>
