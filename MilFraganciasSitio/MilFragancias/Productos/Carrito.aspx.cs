﻿using MilFragancias.MFService;
using MilFragancias.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MilFragancias.Productos
{
    public partial class Carrito : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }
        protected void BindData()
        {
            List<MiniProducto> minis = CarroDeCompras.CapturarProducto().ListaProductos;
            List<ProductoE> productos = new List<ProductoE>();
            ProductoBLClient cli = new ProductoBLClient();
            foreach (var item in minis)
            {
                ProductoE temp = cli.BuscarProducto(item.Id);
                temp.Cantidad = item.Cantidad;
                productos.Add(temp);
            }
            pdcarrito.DataSource = productos;
            pdcarrito.DataBind();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            total.InnerHtml = "₡" + CarroDeCompras.CapturarProducto().SubTotal().ToString();
            base.Render(writer);
        }

        protected String GetImageURL(object imagen)
        {
            if (imagen is ImagenE)
            {
                return "data:image;base64," + Convert.ToBase64String(((ImagenE)imagen)?.Imagen);
            }
            return "https://dummyimage.com/50x50/55595c/fff";
        }

        protected void Seguir_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Productos/Productos.aspx");
        }

        protected void SacarCarrito(object sender, EventArgs e)
        {
            CarroDeCompras carrito = CarroDeCompras.CapturarProducto();


        }
    }
}