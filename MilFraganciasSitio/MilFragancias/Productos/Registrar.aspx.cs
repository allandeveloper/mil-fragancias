﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MilFragancias.MFService;
using MilFragancias.Util;

namespace MilFragancias.Productos
{
    public partial class Registrar : BootstrapPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ProductoBLClient client = new ProductoBLClient();
            tipo.DataTextField = "Tipo";
            tipo.DataValueField = "Id";
            tipo.DataSource = client.MostrarTipos();
            tipo.DataBind();
        }

        protected void btnRun_Click(object sender, EventArgs e)
        {
            try
            {
                ProductoBLClient client = new ProductoBLClient();
                Byte[] imgByte = null;
                if (img.HasFile && img.PostedFile != null)
                {
                    //To create a PostedFile
                    HttpPostedFile File = img.PostedFile;
                    //Create byte Array with file len
                    imgByte = new Byte[File.ContentLength];
                    //force the control to load data in array
                    File.InputStream.Read(imgByte, 0, File.ContentLength);
                }

                ImagenE ima = new ImagenE
                {
                    Imagen = imgByte,
                    Activo = 'A'
                };

                ProductoE prod = new ProductoE
                {
                    Producto = producto.Text.Trim(),
                    Descripcion = descripcion.Text.Trim(),
                    Tipo = new TipoProductoE { Id = Int32.Parse(tipo.SelectedValue) },
                    Cantidad = Int32.Parse(cantidad.Text),
                    Precio = float.Parse(precio.Text),
                    Activo = 'A',
                    Imagen = ima
                };
                client.NuevoProducto(prod);
                Limpiar();
                ShowNotification("Producto Registrado con éxito", WarningType.Success);
            }
            catch (Exception ex)
            {
                ShowNotification("Favor intente nuevamente", WarningType.Danger);
            }
        }

        public void Limpiar()
        {
            foreach (var item in Controls)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Text = "";
                }
                if (item is DropDownList)
                {
                    ((DropDownList)item).SelectedIndex = -1;
                }
            }
        }
    }
}