﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Carrito.aspx.cs" Inherits="MilFragancias.Productos.Carrito" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/carrito.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">CARRITO DE COMPRAS
            </h1>
        </div>
    </section>

    <div class="container mb-4">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Producto</th>
                                <th scope="col">Disponibilidad</th>
                                <th scope="col" class="text-center">Cantidad</th>
                                <th scope="col" class="text-right">Precio</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="pdcarrito" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <img src="<%# GetImageURL(Eval("Imagen")) %>" />
                                        </td>
                                        <td><%# Eval("Producto") %></td>
                                        <td>In stock</td>
                                        <td>
                                            <input class="form-control" type="text" value="<%# Eval("Cantidad") %>" /></td>
                                        <td class="text-right"><%# Eval("Precio") %></td>
                                        <td class="text-right">
                                            <button class="btn btn-sm btn-danger" value="<%# Eval("Id") %>"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><strong>Total</strong></td>
                                <td class="text-right" id="total" runat="server"><strong>346,90 €</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col mb-2">
                <div class="row">
                    <div class="col-sm-12  col-md-6">
                        <button class="btn btn-block btn-light" OnServerClick="Seguir_Click" runat="server">Seguir de compras</button>
                    </div>
                    <div class="col-sm-12 col-md-6 text-right">
                        <button class="btn btn-lg btn-block btn-success text-uppercase">Pagar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
