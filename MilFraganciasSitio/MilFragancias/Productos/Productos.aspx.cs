﻿using MilFragancias.MFService;
using MilFragancias.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MilFragancias.Productos
{
    public partial class Productos : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ProductoBLClient client = new ProductoBLClient();
                cdcatalog.DataSource = client.MostrarTipos().ToList();
                cdcatalog.DataBind();
                List<ProductoE> productos = client.MostrarProductos().ToList();
                pdcatalog.DataSource = productos;
                pdcatalog.DataBind();
                ProductoE last = productos.Last();
            }
        }
        protected void CambiarFiltro()
        {
            Filtro.Text = "test";
        }

        protected String GetImageURL(object imagen)
        {
            if (imagen is ImagenE)
            {
                return "data:image;base64," + Convert.ToBase64String(((ImagenE)imagen)?.Imagen);
            }
            return "https://dummyimage.com/600x400/55595c/fff";
        }

        protected void AgregarCarrito(object sender, EventArgs e)
        {
            CarroDeCompras carrito = CarroDeCompras.CapturarProducto();
            if (sender is Button)
            {
                Button b = sender as Button;
                carrito.Agregar(Convert.ToInt32(b.CommandArgument));
            }
        }
    }
}

