﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="MilFragancias.Productos.Productos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron text-center">
        <h1>PRODUCTOS:
            <asp:Label Text="TODOS" ID="Filtro" runat="server" /></h1>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3">
                <div class="card bg-light mb-3">
                    <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-list"></i>Categorías</div>
                    <ul class="list-group category_block">
                        <asp:Repeater ID="cdcatalog" runat="server">
                            <ItemTemplate>
                                <li  class="list-group-item"><a href="#<%# Eval("Tipo")%>"><%# Eval("Tipo")%></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="card bg-light mb-3">
                    <div class="card-header bg-success text-white text-uppercase">Last product</div>
                    <div class="card-body">
                        <img class="img-fluid" src="" />
                        <h5 class="card-title">Product title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <p class="bloc_left_price">99.00 $</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="row">
                    <asp:Repeater ID="pdcatalog" runat="server">
                        <ItemTemplate>
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="card">
                                    <img class="card-img-top" src="<%# GetImageURL(Eval("Imagen")) %>" alt="Card image cap">
                                    <div class="card-body">
                                        <h4 class="card-title"><a href="product.html" title="View Product"><%# Eval("Producto")%></a></h4>
                                        <p class="card-text"><%# Eval("Descripcion")%></p>
                                        <p class="card-text">Disponibles: <%# Eval("Cantidad")%></p>
                                        <div class="row">
                                            <div class="col">
                                                <p class="btn btn-danger btn-block">₡<%# Eval("Precio")%></p>
                                            </div>
                                            <div class="col">
                                                <asp:Button Text="Agregar" class="btn btn-success btn-block" commandargument=<%# Eval("Id") %>  OnClick="AgregarCarrito" runat="server" />
                                               <%-- <a href="#" class="btn btn-success btn-block" onclick="<%# AgregarCarrito(Eval("Id")) %>">Agregar</a>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div class="col-12">
                        <nav aria-label="...">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
