﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registrar.aspx.cs" Inherits="MilFragancias.Productos.Registrar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/registro.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <br />
    <br />
    <div class="container ">
        <div class="row">
            <div class="col-xl-12 text-center">
                <h2>Registro de Productos</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="name">Producto</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"></div>
                        <asp:TextBox runat="server" type="text" name="producto" class="form-control" ID="producto"
                            placeholder="Hugo Boss" required autofocus></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="name">Descripción</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"></div>
                        <asp:TextBox runat="server" type="text" name="name" class="form-control" ID="descripcion"
                            placeholder="Descripción del producto" TextMode="MultiLine" Rows="5" required autofocus></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="name">Categoría</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"></div>
                        <asp:DropDownList runat="server" type="text" name="name" class="form-control" ID="tipo"
                            placeholder="Floral" required autofocus>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="name">Cantidad</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"></div>
                        <asp:TextBox runat="server" type="text" name="name" class="form-control" ID="cantidad"
                            placeholder="100" required autofocus></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="name">Precio</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"></div>
                        <asp:TextBox runat="server" type="text" name="name" class="form-control" ID="precio"
                            placeholder="10000" required autofocus></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="name">Imagen</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"></div>
                        <asp:FileUpload ID="img" runat="server" />  
                        <asp:Image ID="Image1" runat="server" />
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 field-label-responsive">
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem">
                            <i class=""></i>
                        </div>
                        <asp:Button runat="server" ID="btnRun" class="btn btn-success w-100" title="Registrar." Text="Registrar" OnClick="btnRun_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
