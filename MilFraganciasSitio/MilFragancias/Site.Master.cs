﻿using MilFragancias.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MilFragancias
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            List<MiniProducto> minis = CarroDeCompras.CapturarProducto().ListaProductos;
            carrito.InnerHtml = minis.Count.ToString();

            base.Render(writer);
        }

        protected void VerCarrito(object sender, EventArgs e)
        {
            Response.Redirect("~/Productos/Carrito.aspx");
        }

        protected void IrProductos(object sender, EventArgs e)
        {
            Response.Redirect("~/Productos/Productos.aspx");
        }

        protected void IrNuevoProducto(object sender, EventArgs e)
        {
            Response.Redirect("~/Productos/Registrar.aspx");
        }

        protected void IrIniciarSesion(object sender, EventArgs e)
        {
            Response.Redirect("~/Usuarios/Login.aspx");
        }
    }
}