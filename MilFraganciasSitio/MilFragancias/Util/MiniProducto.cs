﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MilFragancias.Util
{
    public class MiniProducto
    {
        public int Id { get; set; }
        public int Cantidad { get; set; }
        public float Precio { get; set; }

        public MiniProducto(int id)
        {
            Id = id;
        }

        public override bool Equals(object obj)
        {

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return (obj as MiniProducto).Id == Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}