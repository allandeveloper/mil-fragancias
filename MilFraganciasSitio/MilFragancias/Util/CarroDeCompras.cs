﻿using MilFragancias.MFService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MilFragancias.Util
{
    public class CarroDeCompras
    {
        public List<MiniProducto> ListaProductos { get; private set; }

        public static CarroDeCompras CapturarProducto()
        {
            CarroDeCompras _carrito = (CarroDeCompras)HttpContext.Current.Session["MFCarroDeCompras"];
            if (_carrito == null)
            {
                HttpContext.Current.Session["MFCarroDeCompras"] = _carrito = new CarroDeCompras();
            }
            return _carrito;
        }

        protected CarroDeCompras()
        {
            ListaProductos = new List<MiniProducto>();
        }

        public void Agregar(int pIdProducto)
        {
            ProductoBLClient cli = new ProductoBLClient();
            ProductoE temp = cli.BuscarProducto(pIdProducto);
            MiniProducto NuevoProducto = new MiniProducto(pIdProducto) { Precio=temp.Precio};

            if (ListaProductos.Contains(NuevoProducto))
            {
                foreach (MiniProducto item in ListaProductos)
                {
                    if (item.Equals(NuevoProducto))
                    {
                        item.Cantidad++;
                        return;
                    }
                }
            }
            else
            {
                NuevoProducto.Cantidad = 1;
                ListaProductos.Add(NuevoProducto);
            }
        }
        public void EliminarProductos(int pIdProducto)
        {
            MiniProducto eliminaritems = new MiniProducto(pIdProducto);
            ListaProductos.Remove(eliminaritems);
        }
        public void CantidadDeProductos(int pIdProducto, int pCantidad)
        {
            if (pCantidad == 0)
            {
                EliminarProductos(pIdProducto);
                return;
            }
            MiniProducto updateProductos = new MiniProducto(pIdProducto);
            foreach (MiniProducto item in ListaProductos)
            {
                if (item.Equals(updateProductos))
                {
                    item.Cantidad = pCantidad;
                    return;
                }
            }
        }

        public decimal SubTotal()
        {
            decimal subtotal = 0;
            foreach (MiniProducto item in ListaProductos)
            {
                subtotal += Convert.ToDecimal(item.Precio * item.Cantidad);
            }
            return subtotal;
        }
    }
}
