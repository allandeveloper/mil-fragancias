﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MilFragancias.Util
{
    public enum WarningType
    {
        Success,
        Info,
        Warning,
        Danger
    }
}