﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MilFragancias.Util
{
    public class Util
    {
        public static void Limpiar(ControlCollection Control)
        {
            foreach (var item in Control)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Text = "";
                }
                if (item is DropDownList)
                {
                    ((DropDownList)item).SelectedIndex = -1;
                }
            }
        }
    }
}

