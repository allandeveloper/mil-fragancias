﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="MilFragancias.Usuarios.RegistroUsuarios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/registro.css" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <br />
    <br />
    <div class="container ">
        <div class="row">
            <div class="col-xl-12 text-center">
                <h2>Registro de Usuarios</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="name">Usuario</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                        <asp:TextBox runat="server" type="text" name="name" class="form-control" ID="user"
                            placeholder="amurillo" required autofocus></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                    <span class="text-danger align-middle">
                        <!-- Put name validation error messages here -->
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="name">Nombre</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                        <asp:TextBox runat="server" type="text" name="name" class="form-control" ID="name"
                            placeholder="Allan Murillo" required autofocus></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                    <span class="text-danger align-middle">
                        <!-- Put name validation error messages here -->
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="email">Correo electrónico</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
                        <asp:TextBox runat="server" name="email" class="form-control" ID="email"
                            placeholder="sucorreo@ejemplo.com" required autofocus></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                    <span class="text-danger align-middle">
                        <!-- Put e-mail validation error messages here -->
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="password">Contraseña</label>
            </div>
            <div class="col-md-5">
                <div class="form-group has-danger">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
                        <asp:TextBox runat="server" type="password" name="password" class="form-control" ID="password"
                            placeholder="Contraseña" required></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-control-feedback">
                    <span class="text-danger align-middle">
                        <%-- <i class="fa fa-close">Example Error Message</i>--%>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
                <label for="password">Confirmar Contraseña</label>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem">
                            <i class="fa fa-repeat"></i>
                        </div>
                        <asp:TextBox runat="server" type="password" name="password-confirmation" class="form-control"
                            ID="passwordConfirm" placeholder="Contraseña" required></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 field-label-responsive">
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem">
                            <i class=""></i>
                        </div>
                        <asp:Button runat="server" ID="btnRun" OnClick="Registro_Click" class="btn btn-success w-100" title="Registrar." Text="Registrar" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
