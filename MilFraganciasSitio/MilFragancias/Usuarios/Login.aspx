﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MilFragancias.Usuarios.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/login.css" rel="stylesheet" />
    <br />
    <br />
    <div class="login-popup-wrap new_login_popup">
        <div class="col-sm-8">
            <div class="login-popup-heading text-center">
                <h4><i class="fa fa-lock" aria-hidden="true"></i>Iniciar Sesión </h4>
            </div>
            <div class="form-group">
                <asp:TextBox runat="server" type="text" class="form-control" ID="userId" placeholder="Usuario" name="userId"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:TextBox runat="server" type="password" class="form-control" ID="password" placeholder="Contraseña" name="password"></asp:TextBox>
            </div>
            <asp:Button runat="server" type="submit" class="btn btn-default login-popup-btn" name="submit" value="1" Text="Iniciar Sesión" OnClick="Login_Click"></asp:Button>
            <div class="form-group text-center">
                <a class="pwd-forget" href="javascript:void(0)" id="open_forgotPassword">Olvido su contraseña</a>
            </div>
            <div class="text-center">No tienes una cuenta? <a href="#">Click aquí</a></div>
        </div>
    </div>

</asp:Content>
