﻿using MilFragancias.MFService;
using MilFragancias.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MilFragancias.Usuarios
{
    public partial class Login: Util.BootstrapPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login_Click(object sender, EventArgs e)
        {
            UsuarioBLClient client = new UsuarioBLClient();
            UsuarioE usuarioE = new UsuarioE { Usuario = userId.Text, Contrasenna = password.Text };
            usuarioE = client.AuntentificarUsuario(usuarioE);

            if (usuarioE?.Id > 0)
            {
                Session["Login"] = usuarioE;
                ShowNotification("Bienvenido, " + usuarioE.Nombre, WarningType.Success);
                Response.Redirect("~/Productos/Productos.aspx");
            }
            else
            {
                Session["Login"] = new UsuarioE() { Usuario = "Invitado" };
            }
        }
    }
}