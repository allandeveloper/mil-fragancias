﻿using MilFragancias.MFService;
using MilFragancias.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MilFragancias.Usuarios
{
    public partial class RegistroUsuarios : BootstrapPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Registro_Click(object sender, EventArgs e)
        {
            try
            {
                UsuarioBLClient client = new UsuarioBLClient();
                UsuarioE usuarioE = new UsuarioE
                {
                    Id = 0,
                    Usuario = user.Text.Trim(),
                    Nombre = name.Text.Trim(),
                    Correo = email.Text.Trim(),
                    Contrasenna = password.Text.Trim(),
                    Activo = 'A'
                };
                if (client.NuevoUsuario(usuarioE) == 1)
                {
                    Limpiar();
                    ShowNotification("Usuario registrado con éxito", WarningType.Success);
                }
            }
            catch (Exception ex)
            {
                ShowNotification("Favor intente nuevamente", WarningType.Danger);
            }

        }

        public void Limpiar()
        {
            foreach (var item in Controls)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Text = "";
                }
                if (item is DropDownList)
                {
                    ((DropDownList)item).SelectedIndex = -1;
                }
            }
        }
    }
}